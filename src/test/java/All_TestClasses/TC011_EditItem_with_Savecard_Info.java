package All_TestClasses;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;
import org.apache.poi.EncryptedDocumentException;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;

import Admin_Business.A_Base_Class;
import Admin_Business.A_Utility_Class;
import Admin_Business.Add_Service;
import Admin_Business.Add_VenueDetails;
import Admin_Business.Admin_LogOut;
import Admin_Business.Booking_CashPayment;
import Admin_Business.Bussiness_Login1;

import Admin_Business.Create_Booking1_CardPay;
import Admin_Business.Edit_Event;
import Admin_Business.Edit_Addon;
import Admin_Business.Edit_Items;
import Admin_Business.Edit_Date;
import Admin_Business.Edit_Lane;
import Admin_Business.Upfront0_Booking;

public class TC011_EditItem_with_Savecard_Info extends A_Base_Class
{

	// Initialise_Brouser();
	// Webdriver driver


	// create object  logger class
	Logger logger = (Logger) LogManager.getLogger("TC011_EditItem_with_Savecard_Info");

	Bussiness_Login1 Blogin;
	Create_Booking1_CardPay BookingCreation;
	Booking_CashPayment CashPay;
	Edit_Lane Edit_Lane;
	Edit_Date EditDate;
	Edit_Addon Edit_Addon;
	Edit_Items Edit_Item;
	Booking_CashPayment CashPayment;
	Edit_Event EditEvent;
	Upfront0_Booking upfront0;

	@BeforeClass
	public void Open_Brouser (ITestContext Context) throws InterruptedException, EncryptedDocumentException, IOException 
	{
		Initialise_Browser(Context);
		logger.info("Browser opened...!");


		//Initialise_Brouser();

		// object of pom classes
		Blogin=new Bussiness_Login1(driver);
		BookingCreation=new Create_Booking1_CardPay(driver);
		CashPay=new Booking_CashPayment(driver);
		Edit_Lane=new Edit_Lane(driver);
		EditDate=new Edit_Date(driver);
		Edit_Addon=new Edit_Addon(driver);
		Edit_Item=new Edit_Items(driver);
		CashPayment=new Booking_CashPayment(driver);
		EditEvent=new Edit_Event(driver);
		upfront0=new Upfront0_Booking(driver);
	}

	@BeforeMethod
	public void Login() throws InterruptedException, IOException 
	{

		Blogin.Click_Radio();
		Blogin.Enter_UN(A_Utility_Class.getdatafromPF("UN_Business"));
		Blogin.Enter_PWS(A_Utility_Class.getdatafromPF("PWS_Business"));
		Blogin.Click_Login();
		Thread.sleep(5000);
		logger.info("Login credentials are entered...!");

	}

	// Edit the item and make payment through save card --> on admin there is option 
	@Test (testName = "Edit_Item_MakePay_via_SaveCard")
	public void Edit_Item_MakePay_via_SaveCard() throws IOException, InterruptedException 
	{
		try
		{ 
			BookingCreation.ClickCalenderbtn();
			extentTest.info("Click Calander button...!");
			logger.info("Clicked on calendar button...!");
			Thread.sleep(2000);

			BookingCreation.select_Outlet(driver);
			extentTest.info("Outlet selected...!");
			logger.info("Outlet selected...!");
			Thread.sleep(2000);

			BookingCreation.Customer_Name(A_Utility_Class.getdatafromEs(1, 0, "CreateBookings_CardPay"));
			extentTest.info(" Customer Name Enter...!");
			Thread.sleep(2000);
			BookingCreation.EnterEmail(A_Utility_Class.getdatafromEs(1, 1, "CreateBookings_CardPay"));
			extentTest.info("Customer Mail Enter...!");
			logger.info("User information are entered...!");
			Thread.sleep(2000);

			BookingCreation.Click_CreateReservation();
			extentTest.info("Click Create Reservation button...!");
			logger.info("Clicked on create reservation button...!");
			Thread.sleep(2000);

			BookingCreation.Click_Calendersymbol();
			extentTest.info("Click on calander symbol...! ");
			BookingCreation.Click_nextArrow();
			extentTest.info("Click on nextMonth Arrow on calander");
			BookingCreation.SelectDate();
			extentTest.info("Select Date for booking...!");
			logger.info("Date selected for booking...!");
			Thread.sleep(3000);

			BookingCreation.Selectplan();
			extentTest.info("Select Plan for booking...!");
			logger.info("Enable Package type plan selected for booking...!");

			// created on 5 to 7 am--> 6th slot
			Thread.sleep(3000);
			BookingCreation.SelectDuration();
			extentTest.info("Select Duration for booking...!");
			Thread.sleep(3000);
			BookingCreation.SelectPackage(driver);
			extentTest.info("Select Package for booking...!");
			Thread.sleep(3000);
			BookingCreation.SelectaddGuest(driver);
			extentTest.info("Select additional Guest for booking...!");
			logger.info("Package/Additional guests are selected...!");

			Thread.sleep(3000);

			BookingCreation.Click_ProceedToPay();
			extentTest.info("Click on Proceed to pay button...!");
			logger.info("Clicked on proceed to pay button...!");
			Thread.sleep(5000);

			BookingCreation.Select_CardPymentMethod();
			extentTest.info("Select Card payment method for booking...!");
			logger.info("Pay via Card option selected for payment...!");
			Thread.sleep(5000);
			
			BookingCreation.Click_Tipcrossbtn();
			extentTest.info("Click on tip cross button...!");
			logger.info("Clicked on tip cross button...!");
			Thread.sleep(2000);

			// To handle iframe element
			driver.switchTo().frame("__teConnectSecureFrame");
			BookingCreation.EnterCardNo(A_Utility_Class.getdatafromEs(1, 2, "CreateBookings_CardPay"));
			extentTest.info("Enter card no...!");
			Thread.sleep(3000);
			BookingCreation.EnterExpiryDate(A_Utility_Class.getdatafromEs(1, 3, "CreateBookings_CardPay"));
			extentTest.info("Enter Expiry Date...!");
			Thread.sleep(3000);
			BookingCreation.Entercvc(A_Utility_Class.getdatafromEs(1, 4, "CreateBookings_CardPay"));
			extentTest.info("Enter CVC...!");
			logger.info("Card details entered successfully...!");
			Thread.sleep(3000);

			//To switch focus of selenium on main page
			driver.switchTo().defaultContent();
			BookingCreation.ClickPayBtn();
			extentTest.info("Click Pay Button...!");
			logger.info("Clicked on pay button...!");
			Thread.sleep(3000);

	/*		Edit_Addon.Click_RefreshBtn();
			extentTest.info("Click Refresh Button...!");
			logger.info("Clicked on refresh button to change the status...!");

			CashPay.Click_ReceivedCheckBox();
			logger.info("Clicked on Payment Received checkbox...!");
			extentTest.info("Click Payment Received checkbox...!");
			Thread.sleep(2000);

			CashPay.Click_ConfirmBtn();
			extentTest.info("Click Confirm button...!");
			logger.info("Clicked on confirm button...!");
			Thread.sleep(2000);*/

			//get the booking Id
			String BookingId=BookingCreation.getBookingId(driver);
			System.out.println("Booking Id is:-" + BookingId);


			//get booking status
			String Status=upfront0.getText_BookingStatus();
			//Assertion applied to verify status of the booking after payment
			Assert.assertEquals(Status, "Successful", "Status of the booking is");
			extentTest.info(MarkupHelper.createLabel("Booking status assertion is passed...!",
					ExtentColor.GREEN));
			System.out.println("Booking status assertion is passed...!");
			logger.info("Booking status assertion is passed...!");


			BookingCreation.Click_SubmitButton();
			extentTest.info("Click Submit Button...!");
			logger.info("Clicked on submit button...!");
			Thread.sleep(5000);

			extentTest.info(MarkupHelper.createLabel("Booking created successfully...!", ExtentColor.ORANGE));
			System.out.println("Booking created successfully...!");
			logger.info("Booking created successfully...!");
			Thread.sleep(3000);

			//Click created booking to update the  Items
			//			Edit_Item.Click_Created_Booking(driver);
			//			extentTest.info("Click Created booking for Edit");
			//			logger.info("Clicked on Created booking to edit items");
			//			Thread.sleep(5000);

			//click on view reservation button
			EditEvent.Click_ViewReservation_Btn();
			extentTest.info("Click View Reservation Button...!");
			logger.info("Clicked on View Reservation button...!");
			Thread.sleep(3000);

			BookingCreation.EnterBookingId(BookingId);
			Thread.sleep(3000);


			EditEvent.Click_BookingArrow();
			extentTest.info("Click on booking arrow...!");
			logger.info("Clicked on Booking arrow...!");
			Thread.sleep(3000);


			EditDate.Click_EditButton();
			extentTest.info("Click Edit Button...!");
			logger.info("Clicked on edit button...!");
			Thread.sleep(7000);

			Edit_Item.Select_Items(driver);
			Thread.sleep(7000);
			BookingCreation.SelectaddGuest(driver);
			extentTest.info("Sale item and Additional guest are selected...!");
			logger.info("Sale item and Additional guest are selected...!");
			System.out.println("Sale item and additional guest are selected...!");
			Thread.sleep(7000);


			Edit_Addon.Click_UpdateBookingBTN();
			extentTest.info("Click Update Booking Button...!");
			logger.info("Clicked on update booking button...!");

			Edit_Addon.Click_ConfirmBTN();
			extentTest.info("Click Confirm Button...!");
			logger.info("Clicked on Confirm Button...!");
			Thread.sleep(2000);

			BookingCreation.Select_CardPymentMethod();
			extentTest.info("Select Card payment method for booking...!");
			logger.info("Card payment method selected for payment...!");
			Thread.sleep(5000);
			
			BookingCreation.Click_Tipcrossbtn();
			extentTest.info("Click on tip cross button...!");
			logger.info("Clicked on tip cross button...!");
			Thread.sleep(2000);

			Edit_Addon.Click_Payvia_savedcard();
			extentTest.info("Click Pay Via Save Card Option...!");
			logger.info("Clicked on Pay Via Save Card Option...!");
			Thread.sleep(3000);

			Edit_Addon.payupdate();
			extentTest.info("Click pay Button...!");
			logger.info("Clicked on Pay button...!");
			Thread.sleep(2000);

	/*		Edit_Addon.Click_RefreshBtn();
			extentTest.info("Click Refresh Button...!");
			logger.info("Clicked on refresh button...!");

			CashPayment.Click_ReceivedCheckBox();
			extentTest.info("Click payment Received Check Box...!");
			logger.info("Clicked on payment Received check box...!");
			Thread.sleep(2000);

			CashPayment.Click_ConfirmBtn();
			extentTest.info("Click Confirm Button...!");
			logger.info("Clicked on Confirm Button...!"); */

			Assert.assertEquals(driver.findElement(By.xpath("//div[@id='notistack-snackbar']"))
					.isDisplayed(), true);
			
			extentTest.info(MarkupHelper.createLabel("Updated item verifying assertion is passed...!",
					ExtentColor.GREEN));
			System.out.println("Updated item verifying assertion is passed...!");
			logger.info("Updated item verifying assertion is passed...!");
			Thread.sleep(3000);


			BookingCreation.Click_SubmitButton();
			extentTest.info("Click Submit Button...!");
			logger.info("Clicked on submit button...!");

			extentTest.info(MarkupHelper.createLabel("Items updated successfully", ExtentColor.ORANGE));
			System.out.println("Items updated successfully...!");
			logger.info("Items updated successfully...!");
		}

		catch(Exception e) 
		{
			e.getStackTrace();
			String getCause = e.getLocalizedMessage();
			System.out.println("issue cause is :"+getCause);
			extentTest.fail("TC011_EditItem_with_Savecard_Info test case is fail due to" + getCause);
			logger.error("TC011_EditItem_with_Savecard_Info test case is fail due to " + getCause);
		}


	}


}
