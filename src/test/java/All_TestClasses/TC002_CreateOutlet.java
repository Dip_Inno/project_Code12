package All_TestClasses;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;
import org.apache.poi.EncryptedDocumentException;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;

import Admin_Business.A_Base_Class;
import Admin_Business.A_Utility_Class;
import Admin_Business.Add_Business_Page1;
import Admin_Business.Add_Service;
import Admin_Business.Admin_LogOut;
import Admin_Business.Admin_Login1;
import Admin_Business.Bussiness_Login1;
import Admin_Business.Create_Bussiness;
import Admin_Business.Create_Outlet;


public class TC002_CreateOutlet extends A_Base_Class

{   
	Logger logger = (Logger) LogManager.getLogger("TC002_CreateOutlet");
	

	Admin_Login1 Admin;
	Add_Business_Page1 AddB;
	Create_Bussiness CreateB;
	Admin_LogOut Log;
	Create_Outlet CreatOutlet;
	

	@BeforeClass
	public void Open_Brouser (ITestContext Context) throws InterruptedException, EncryptedDocumentException, IOException 
	{
		Initialise_Browser(Context);
		logger.info("Browser opened...!");
		
		//Initialise_Brouser();

		// object of pom classes
		Admin=new Admin_Login1(driver);
		AddB=new Add_Business_Page1(driver);
		CreateB=new Create_Bussiness(driver);
		Log=new Admin_LogOut(driver);
	    CreatOutlet=new Create_Outlet(driver);
	    
	}

	@BeforeMethod
	public void Login() throws InterruptedException, IOException 
	{
		
		Admin.EnterUN(A_Utility_Class.getdatafromPF("UN"));
		Admin.EnterPWS(A_Utility_Class.getdatafromPF("PWS"));
		Admin.ClickLogin();
		logger.info("Login credentials are entered...!");
		Thread.sleep(2000);


	}

	//Admin user should able to create outlet in the bussiness
	
	@Test (testName = "Outlet_Creation")
	public void Outlet_Creation() throws EncryptedDocumentException, IOException, InterruptedException, InvocationTargetException 
	{
		
		
		try {
			
		AddB.ClickBusinesses();
		extentTest.info("Click Bussiness");
		logger.info("Clicked bussiness button...!");
		Thread.sleep(2000);
		
		CreatOutlet.EnterMail(A_Utility_Class.getdatafromEs( 1, 3, "Business_Data"));
		extentTest.info("Search Mail");
		logger.info("Entered mail and search for bussiness account...!");
		Thread.sleep(2000);
		
		CreatOutlet.clickSearchbtn();
		extentTest.info("Click Search Button");
		Thread.sleep(2000);
		
		CreatOutlet.Click_Arrow();
		extentTest.info("Click On Bussiness Arrow");
		logger.info("Clicked On Bussiness Arrow...!");
		Thread.sleep(2000);
		
		CreatOutlet.clcikAddOutlet();
		extentTest.info("Click on Add Outlet Button");
		logger.info("Clicked On Add Outlet Button...!");
		Thread.sleep(2000);
		
		CreatOutlet.EnterOutletName(A_Utility_Class.getdatafromEs( 1, 1, "Outlet_Data"));
		extentTest.info("Enter Outlet Name");
		logger.info("Outlet name Entered...!");
		Thread.sleep(2000);
		
		CreatOutlet.EnterOutletEmail(A_Utility_Class.getdatafromEs(1, 3, "Business_Data")); 
		extentTest.info("Enter Outlet Email");
		logger.info("Outlet name entered...!");
		Thread.sleep(2000);
		
		CreatOutlet.EnterFlatno(A_Utility_Class.getdatafromEs( 1, 2, "Outlet_Data"));
		extentTest.info("Enter Flat No.");
		logger.info("Flat No. entered...!");
		Thread.sleep(2000);
		
		
		CreatOutlet.Select_State(driver);
		extentTest.info("Select State");
		logger.info("State selected...!");
		Thread.sleep(5000);
		
		CreatOutlet.Select_City(driver);
		extentTest.info("Select City");
		logger.info("City selected...!");
		Thread.sleep(2000);
		
		CreatOutlet.Enter_ZipCode(A_Utility_Class.getdatafromEs(1, 3, "Outlet_Data"));
		extentTest.info("Enter Zipcode");
		logger.info("Zip Code entered...!");
		Thread.sleep(2000);
		
		CreatOutlet.SelectRegion(driver);
		extentTest.info("Select Region");
		logger.info("Region selected...!");
		Thread.sleep(2000);
		
		CreateB.ClickCreate();
		extentTest.info("Click Create Button");
		logger.info("Click Create Button...!");
		
		logger.info("Outlet created successfully...!");
		System.out.println("Outlet created successfully...!");
		extentTest.info("Outlet created successfully...!");
		
		

		if(driver.getPageSource().contains("Sucessfully Created")) 
		{
			extentTest.info(MarkupHelper.createLabel("Outlet has been successfully created..!", ExtentColor.ORANGE));
			System.out.println("Outlet has been successfully created..!");
			logger.info("Outlet has been successfully created..!");
		}
		
		
		else if(driver.getPageSource().contains("Name is required"))
		{
			extentTest.fail(MarkupHelper.createLabel("Outlet is not created due to Name is required...!", ExtentColor.ORANGE));
			System.out.println("Outlet is not created due to Name is required...!");
			logger.info("Outlet is not created due to Name is required...!");
			Assert.fail("Name is required");
		
		}
		
		else if(driver.getPageSource().contains("Flat/House no. is required"))
		{
			extentTest.fail(MarkupHelper.createLabel("Outlet is not created due to Flat/House no. is required...!", ExtentColor.ORANGE));
			System.out.println("Outlet is not created due to Flat/House no. is required...!");
			logger.info("Outlet is not created due to Flat/House no. is required...!");
			Assert.fail("Flat/House no. is required");
		}
		else if(driver.getPageSource().contains("City is required"))
		{
			extentTest.fail(MarkupHelper.createLabel("Outlet is not created due to City is required...!", ExtentColor.ORANGE));
			System.out.println("Outlet is not created due to City is required...!");
			logger.info("Outlet is not created due to City is required...!");
			Assert.fail("City is required");
		}
		else if(driver.getPageSource().contains("State is required"))
		{
			extentTest.fail(MarkupHelper.createLabel("Outlet is not created due to State is required...!", ExtentColor.ORANGE));
			System.out.println("Outlet is not created due to Name is required...!");
			logger.info("Outlet is not created due to State is required...!");
			Assert.fail("State is required");
		}
		else if(driver.getPageSource().contains("Zip Code is required"))
		{
			extentTest.fail(MarkupHelper.createLabel("Outlet is not created due to Zip Code is required...!", ExtentColor.ORANGE));
			System.out.println("Outlet is not created due to Zip Code is required...!");
			logger.info("Outlet is not created due to Zip Code is required...!");
			Assert.fail("Zip Code is required");
		}
		else if(driver.getPageSource().contains("Region is Required"))
		{
			extentTest.fail(MarkupHelper.createLabel("Outlet is not created due to Region is required...!", ExtentColor.ORANGE));
			System.out.println("Outlet is not created due to Region is required...!");
			logger.info("Outlet is not created due to Region is required...!");
			Assert.fail("Region is required");
		}
		
		
		}
		
		
		catch(Exception e) 
		{
			e.getStackTrace();
	         String getCause = e.getLocalizedMessage();
	         System.out.println("issue cause is :"+getCause);
	         extentTest.fail("TC002_CreateOutlet Test case is Fail due to" + getCause);
		}
		
	       
	       
		
	
		      
	}

}
