package All_TestClasses;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;
import org.apache.poi.EncryptedDocumentException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;

import Admin_Business.A_Base_Class;
import Admin_Business.A_Utility_Class;
import Admin_Business.Add_Service;
import Admin_Business.Add_VenueDetails;
import Admin_Business.Admin_LogOut;
import Admin_Business.Bussiness_Login1;

public class TC003_Add_ServiceCategory extends A_Base_Class
{
	// Initialise_Brouser();
	// Webdriver driver

	//Create object of logger class
	Logger logger=(Logger) LogManager.getLogger("TC003_Add_ServiceCategory");

	// globally declared objects
	Bussiness_Login1 Blogin;
	Add_Service  AddService;
	Add_VenueDetails AddVenue;
	Admin_LogOut Logout;

	@BeforeClass
	public void Open_Brouser (ITestContext Context) throws InterruptedException, EncryptedDocumentException, IOException 
	{
		Initialise_Browser(Context);
		logger.info("Browser Opened...!");

		//Initialise_Brouser();

		// object of pom classes

		Blogin=new Bussiness_Login1(driver);
		AddService=new Add_Service(driver);
		AddVenue=new Add_VenueDetails(driver);
		Logout=new Admin_LogOut(driver);

	}

	@BeforeMethod
	public void Login() throws InterruptedException, IOException 
	{

		Blogin.Click_Radio();
		Blogin.Enter_UN(A_Utility_Class.getdatafromEs(1, 3, "Business_Data"));
		Blogin.Enter_PWS(A_Utility_Class.getdatafromEs(1, 9, "Business_Data"));
		Blogin.Click_Login();
		logger.info("Login credentials are entered...!");

	}

	@Test (testName = "ADD_Service")
	public void ADD_Service() throws IOException, InterruptedException 
	{

		try 
		{
			AddService.clickOutlet();
			extentTest.info("Click Outlet button");
			logger.info("Clicked on outlet button...!");
			Thread.sleep(5000);

			// search outlet 
			
			AddService.clickArrow(driver);
			extentTest.info("Click On Arrow");
			logger.info("Login credentials are entered...!");
			Thread.sleep(2000);

			AddService.click_Addservice();
			extentTest.info("Click On Add Service Button");
			logger.info("Clicked On Add Service Button...!");

			AddService.click_EntRadio();
			extentTest.info("Click On Service Radio Button ");
			logger.info("Clicked On Service Radio Button...!");

			AddService.click_Confirmbtn();
			extentTest.info("Click On Confirm");
			logger.info("Clicked On Confirm...!");
			Thread.sleep(2000);

			AddService.select_Uber();
			extentTest.info("Uber selected");
			logger.info("Uber selected...!");
			Thread.sleep(2000);

			AddService.select_Status();
			extentTest.info("Status active selected");
			logger.info("Status active selected...!");
			Thread.sleep(2000);

			AddService.click_Savebtn();
			extentTest.info("Click On Save");
			logger.info("Clicked On Save...!");
			Thread.sleep(3000);

			extentTest.info(MarkupHelper.createLabel("Service configuration has been successfully saved..!", ExtentColor.ORANGE));
			System.out.println("Service configuration has been successfully saved..!");
			logger.info("Service configuration has been successfully saved..!");

		}
		catch(Exception e) 
		{
			e.getStackTrace();
			String getCause = e.getLocalizedMessage();
			System.out.println("issue cause is :"+getCause);
			extentTest.fail("TC003_Add_ServiceCategory test case is fail due to" + getCause);
		}

	}




}
