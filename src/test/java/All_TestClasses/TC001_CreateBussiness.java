package All_TestClasses;

import java.awt.AWTException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;
import org.apache.poi.EncryptedDocumentException;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;

import Admin_Business.A_Base_Class;
import Admin_Business.A_Utility_Class;
import Admin_Business.Add_Business_Page1;
import Admin_Business.Add_Service;
import Admin_Business.Admin_LogOut;
import Admin_Business.Admin_Login1;
import Admin_Business.Booking_PaymentVia_Email;
import Admin_Business.Bussiness_Login1;
import Admin_Business.Create_Bussiness;
import Admin_Business.Create_Outlet;


public class TC001_CreateBussiness extends A_Base_Class

{ 

	Logger logger = (Logger) LogManager.getLogger("TC001_CreateBussiness");

	Admin_Login1 Admin;
	Add_Business_Page1 AddB;
	Create_Bussiness CreateB;
	Admin_LogOut Log;
	Create_Outlet CreatOutlet;
	Booking_PaymentVia_Email PayviaEmail;
	Bussiness_Login1 Blogin;


	@BeforeClass
	public void Open_Brouser (ITestContext Context) throws InterruptedException, EncryptedDocumentException, IOException 
	{
		Initialise_Browser(Context);
		logger.info("Browser opened...!");

		Admin=new Admin_Login1(driver);
		AddB=new Add_Business_Page1(driver);
		CreateB=new Create_Bussiness(driver);
		Log=new Admin_LogOut(driver);
		CreatOutlet=new Create_Outlet(driver);
		PayviaEmail=new Booking_PaymentVia_Email(driver); 
		Blogin=new Bussiness_Login1(driver);

	}

	@BeforeMethod
	public void Login() throws InterruptedException, IOException 
	{

		Admin.EnterUN(A_Utility_Class.getdatafromPF("UN"));
		Admin.EnterPWS(A_Utility_Class.getdatafromPF("PWS"));
		Admin.ClickLogin();
		logger.info("Login credentials are entered...!");


	}
	// TC1= Admin user should able to create Business
	@Test (testName = "Business_Creation")
	public void Business_Creation() throws EncryptedDocumentException, IOException, InterruptedException, AWTException 
	{ 

		try {	AddB.ClickBusinesses();
		extentTest.info("Click Bussiness button ");
		logger.info("Clicked on bussiness button");

		AddB.ClickAddBusiness();
		extentTest.info("Click Add Bussiness button ");
		logger.info("Clicked on add bussiness button...!");

		//CreateB.ClickUploadbtn();
		//A_Utility_Class.uploadimg();
		CreateB.Enter_BussinessName(A_Utility_Class.getdatafromEs(1, 0, "Business_Data" ));
		extentTest.info("Enter Bussiness Name ");
		logger.info("Bussiness name entered...!");

		CreateB.Enter_FirstName(A_Utility_Class.getdatafromEs(1, 1, "Business_Data"));
		extentTest.info("Enter First name ");
		logger.info("First name entered...!");

		CreateB.Enter_LastName(A_Utility_Class.getdatafromEs(1, 2, "Business_Data"));
		extentTest.info("Enter Last Name");
		logger.info("Last name entered...!");

		CreateB.Enter_Email(A_Utility_Class.getdatafromEs(1, 3, "Business_Data"));
		extentTest.info("Enter Email");
		logger.info("Email entered...!");

		CreateB.Enter_Street(A_Utility_Class.getdatafromEs(1, 4, "Business_Data"));
		extentTest.info("Enter Street");
		logger.info("Street entered...!");

		CreateB.Enter_City(A_Utility_Class.getdatafromEs(1, 5, "Business_Data"));
		extentTest.info("Enter City");
		logger.info("City name entered...!");

		CreateB.Select_Contry();
		extentTest.info("Select Country");
		logger.info("Country name entered...!");

		CreateB.Enter_State(A_Utility_Class.getdatafromEs(1, 6, "Business_Data"));
		extentTest.info("Enter State");
		logger.info("State entered...!");

		CreateB.Enter_Zip(A_Utility_Class.getdatafromEs(1, 7, "Business_Data"));
		extentTest.info("Enter Zip");
		logger.info("Zip Code entered...!");

		CreateB.ClickCreate();
		extentTest.info("Click Create button ");
		logger.info("Clicked on create button ...!");


		if(driver.getPageSource().contains("Successfully Created")) 
		{

			extentTest.info(MarkupHelper.createLabel("Bussiness has been created sucessfully...!", ExtentColor.ORANGE));
			System.out.println("Bussiness has been created sucessfully...!");
			logger.info("Bussiness has been created sucessfully...!");

		}

		else if(driver.getPageSource().contains("First Name is required"))
		{

			extentTest.fail(MarkupHelper.createLabel("Bussiness not created due to  First Name is required ...!", ExtentColor.ORANGE));
			System.out.println("Bussiness not created due to  First Name is required ...!");
			logger.info("Bussiness not created due to  First Name is required ...!");
			Assert.fail("First Name is required");
		}
		else if(driver.getPageSource().contains("Last Name is required"))
		{

			extentTest.fail(MarkupHelper.createLabel("Bussiness not created due to  Last Name is required ...!", ExtentColor.ORANGE));
			System.out.println("Bussiness not created due to  Last Name is required ...!");
			logger.info("Bussiness not created due to  Last Name is required ...!");
			Assert.fail("Last Name is required");

		}
		else if(driver.getPageSource().contains("Email is required"))
		{

			extentTest.fail(MarkupHelper.createLabel("Bussiness not created due to  Email  is required ...!", ExtentColor.ORANGE));
			System.out.println("Bussiness not created due to Email is required ...!");
			logger.info("Bussiness not created due to  Email is required ...!");
			Assert.fail("Email is required");
		}
		else if(driver.getPageSource().contains("Flat/House no. is required"))
		{

			extentTest.fail(MarkupHelper.createLabel("Bussiness not created due to  Flat/House no.  is required ...!", ExtentColor.ORANGE));
			System.out.println("Bussiness not created due to Flat/House no. is required ...!");
			logger.info("Bussiness not created due to  Flat/House no. is required ...!");
			Assert.fail("Flat/House no. is required");
		}
		else if(driver.getPageSource().contains("City is required"))
		{

			extentTest.fail(MarkupHelper.createLabel("Bussiness not created due to  City  is required ...!", ExtentColor.ORANGE));
			System.out.println("Bussiness not created due to City is required ...!");
			logger.info("Bussiness not created due to  City is required ...!");
			Assert.fail("City is required");
		}
		else if(driver.getPageSource().contains("State is required"))
		{

			extentTest.fail(MarkupHelper.createLabel("Bussiness not created due to  State  is required ...!", ExtentColor.ORANGE));
			System.out.println("Bussiness not created due to State is required ...!");
			logger.info("Bussiness not created due to  State is required ...!");
			Assert.fail("State is required");
		}
		else if(driver.getPageSource().contains("Zip Code is required"))
		{

			extentTest.fail(MarkupHelper.createLabel("Bussiness not created due to  Zip Code  is required ...!", ExtentColor.ORANGE));
			System.out.println("Bussiness not created due to Zip Code is required ...!");
			logger.info("Bussiness not created due to  Zip Code is required ...!");
			Assert.fail("Zip Code is required");
		}
		else if(driver.getPageSource().contains("Business name is required"))
		{

			extentTest.fail(MarkupHelper.createLabel("Bussiness not created due to  Business name  is required ...!", ExtentColor.ORANGE));
			System.out.println("Bussiness not created due to Business name is required ...!");
			logger.info("Bussiness not created due to  Business name is required ...!");
			Assert.fail("Business name is required");
		}

		else if(driver.getPageSource().contains("Business already registered, please try with other email"))
		{

			extentTest.fail(MarkupHelper.createLabel("Business already registered, please try with other email...!", ExtentColor.ORANGE));
			System.out.println("Business already registered, please try with other email...!");
			logger.info("Business already registered, please try with other email...!");
			Assert.fail("Business already registered, please try with other email");
		}

		// navigates to yop mail for verification 
		PayviaEmail.NavigateTO_YopMail(driver);
		extentTest.info("Navigated to yopmail.com");
		logger.info("Navigated to yopmail.com...!");
		Thread.sleep(5000);

		PayviaEmail.Entername_Login(A_Utility_Class.getdatafromEs(1, 8, "Business_Data"));
		extentTest.info("Enter UN for yopmail");
		Thread.sleep(5000);
		
		PayviaEmail.Click_mailArrow();
		extentTest.info("Click on next arrow on yopmail");
		logger.info("Logged in into yop mail account...!");
		Thread.sleep(5000);

		// To handle iframe element
		driver.switchTo().frame("ifmail");

		CreateB.Click_Verificationbtn();  // click link inside mail
		extentTest.info("Clicked on verification button...!");
		logger.info("Clicked on verification button...!");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);// wait

		//To switch focus of selenium on main page
		driver.switchTo().parentFrame();
		Thread.sleep(5000);

		// need to handle window pop-up
		Set<String>  AllIds=driver.getWindowHandles();
		ArrayList<String> ar=new  ArrayList<String>(AllIds);
		String WindowPopId=ar.get(1);
		// To switch focus window pop-up
		driver.switchTo().window(WindowPopId);

		CreateB.EnterPass(A_Utility_Class.getdatafromEs(1, 9, "Business_Data"));
		Thread.sleep(2000);
		extentTest.info("Password entered for newly created bussiness...!");
		logger.info("Password entered for newly created bussiness...!");

		CreateB.Enter_ConfirmPass(A_Utility_Class.getdatafromEs(1, 9, "Business_Data"));
		Thread.sleep(2000);
		extentTest.info("Confirm password entered for newly created bussiness...!");
		logger.info("Confirm password entered for newly created bussiness...!");

		CreateB.Click_Createpass();
		extentTest.info("Clicked on create password btn...!");
		logger.info("licked on create password btn...!");
		Thread.sleep(5000);

		//login using new bussiness 
		CreateB.Click_Profile();
		Thread.sleep(2000);
		CreateB.Click_Logout();
		extentTest.info("Existing user logout successfully...!");
		logger.info("PExisting user logout successfully...!");
		Thread.sleep(2000);

		Blogin.Click_Radio();
		Blogin.Enter_UN(A_Utility_Class.getdatafromEs(1, 3, "Business_Data"));
		Blogin.Enter_PWS(A_Utility_Class.getdatafromEs(1, 9, "Business_Data"));
		Blogin.Click_Login();
		Thread.sleep(5000);
		extentTest.info("Newely created bussiness user login successfully...!");
		logger.info("Newely created bussiness user login successfully...!");
		System.out.println("Newely created bussiness user login successfully...!");



		Boolean  ActualResult=driver.findElement(By.xpath("//img[@alt='Logo']")).isDisplayed();
		Boolean  ExpectedResult=true;
		Assert.assertEquals(ActualResult, ExpectedResult);

		}


		catch(Exception e)
		{
			e.getStackTrace();
			String getCause = e.getLocalizedMessage();
			System.out.println("issue cause is :"+getCause);
			extentTest.fail("TC001_CreateBussiness fails due to" + getCause);
		}

	}
}
