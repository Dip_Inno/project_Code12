package All_TestClasses;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;
import org.apache.poi.EncryptedDocumentException;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;

import Admin_Business.A_Base_Class;
import Admin_Business.A_Utility_Class;
import Admin_Business.Add_Service;
import Admin_Business.Add_VenueDetails;
import Admin_Business.Admin_LogOut;
import Admin_Business.Booking_CashPayment;
import Admin_Business.Bussiness_Login1;

import Admin_Business.Create_Booking1_CardPay;
import Admin_Business.Create_Bookings_traffic;
import Admin_Business.Edit_Event;
import Admin_Business.Edit_Date;
import Admin_Business.Edit_Lane;
import Admin_Business.Edit_Time;
import Admin_Business.Edit_Time_Updatesame;
import Admin_Business.Upfront0_Booking;

public class TC014_EditTime_UpdateSameTime extends A_Base_Class
{
	// Initialise_Brouser();
	// Webdriver driver

	// create object  logger class
	Logger logger = (Logger) LogManager.getLogger("TC014_EditTime_UpdateSameTime");

	Bussiness_Login1 Blogin;
	Create_Booking1_CardPay BookingCreation;
	Booking_CashPayment CashPayment;
	Edit_Date EditDate;
	Edit_Lane Edit_Lane;
	Edit_Time  EditTime;
	Edit_Time_Updatesame EditTime_UpdateSame;
	Create_Bookings_traffic Create_Multi_Bookings;
	Edit_Event EditEvent;
	Upfront0_Booking upfront0;

	@BeforeClass
	public void Open_Brouser (ITestContext Context) throws InterruptedException, EncryptedDocumentException, IOException 
	{
		Initialise_Browser(Context);
		logger.info("Browser opened...!");

		//Initialise_Brouser();

		// object of pom classes
		Blogin=new Bussiness_Login1(driver);
		BookingCreation=new Create_Booking1_CardPay(driver);
		CashPayment=new Booking_CashPayment(driver);
		EditDate=new Edit_Date(driver);
		Edit_Lane=new Edit_Lane(driver);
		EditTime=new Edit_Time(driver);
		EditTime_UpdateSame=new Edit_Time_Updatesame(driver);
		Create_Multi_Bookings=new Create_Bookings_traffic(driver);
		EditEvent=new Edit_Event(driver);
		upfront0=new Upfront0_Booking(driver);
	}

	@BeforeMethod
	public void Login() throws InterruptedException, IOException 
	{

		Blogin.Click_Radio();
		Blogin.Enter_UN(A_Utility_Class.getdatafromPF("UN_Business"));
		Blogin.Enter_PWS(A_Utility_Class.getdatafromPF("PWS_Business"));
		Blogin.Click_Login();
		Thread.sleep(5000);
		logger.info("Login credentials are entered...!");

	}

	// User should able to Edit time and update the booking on previous time  
	@Test  (testName = "Admin_Edit_UpdateTime")
	public void Admin_Edit_UpdateTime() throws IOException, InterruptedException 
	{

		try 
		{
			//create booking
			// on 5 AM--> 8thslot
			BookingCreation.ClickCalenderbtn();
			extentTest.info("Click Calander button...!");
			logger.info("Clicked on calendar button...!");
			Thread.sleep(2000);

			BookingCreation.select_Outlet(driver);
			extentTest.info("Outlet selected...!");
			logger.info("Outlet selected...!");
			Thread.sleep(2000);

			BookingCreation.Customer_Name(A_Utility_Class.getdatafromEs(1, 0, "CreateBookings_CardPay"));
			extentTest.info(" Customer Name Enter...! ");
			Thread.sleep(2000);
			BookingCreation.EnterEmail(A_Utility_Class.getdatafromEs(1, 1, "CreateBookings_CardPay"));
			extentTest.info("Customer Mail Enter...! ");
			logger.info("User information are entered...!");
			Thread.sleep(3000);

			BookingCreation.Click_CreateReservation();
			extentTest.info("Click Create Reservation button...!");
			logger.info("Clicked on create reservation button...!");


			/*EditTime.ScrollTo5AM(driver);

			Thread.sleep(5000);
			EditTime.Click5AMCell();//5Am
			extentTest.info("Cell click  on calander");
			logger.info("Clicked on 5 AM cell on calander...!");*/
			Thread.sleep(3000);


			EditTime.Edit_Time(driver, A_Utility_Class.getdatafromEs(1, 1, "EditTime"));//5Am
			Thread.sleep(2000);
			Create_Multi_Bookings.Enter_Minutes(driver, A_Utility_Class.getdatafromEs(1, 3, "multiple Booking creation"));
			Thread.sleep(2000);
			Create_Multi_Bookings.SelectNoon_StartTime(driver, A_Utility_Class.getdatafromEs(1, 2, "EditBooking"));
			extentTest.info("Time entered to create booking...!");
			logger.info("Time entered to create booking...!");
			Thread.sleep(5000);

			BookingCreation.Click_Calendersymbol();
			extentTest.info(" click on calander symbol...!");
			BookingCreation.Click_nextArrow();
			extentTest.info("Click on nextMonth Arrow on calander...!");
			BookingCreation.SelectDate();
			extentTest.info("Select Date for booking...!");
			logger.info("Date selected for booking...!");
			Thread.sleep(3000);

			EditTime.Select_PerHourplan();
			extentTest.info("Select Plan for booking...!");
			logger.info("Per Hour type plan selected for booking...!");
			Thread.sleep(2000);

			EditTime.Select_1Hour();
			extentTest.info("Select 1hour...! ");
			Thread.sleep(2000);
			EditTime.Select_Guest(driver);
			extentTest.info("Select Guest...!");
			Thread.sleep(2000);
			EditTime.SelectItems();
			extentTest.info("Select Item...!");
			logger.info("Hour/Guest/Items are selected...!");
			Thread.sleep(5000);

			BookingCreation.Click_ProceedToPay();
			extentTest.info("Click on Proceed to pay button...!");
			logger.info("Clicked on proceed to pay button...!");
			Thread.sleep(5000);

			CashPayment.Select_CashPymentMethod();
			extentTest.info("Select Cash payment for booking...!");
			logger.info("Payment via cash option selected for payment...!");
			Thread.sleep(2000);
			
			BookingCreation.Click_Tipcrossbtn();
			extentTest.info("Click on tip cross button...!");
			logger.info("Clicked on tip cross button...!");
			Thread.sleep(2000);

			CashPayment.Click_ReceivedCheckBox();
			extentTest.info("Click Payment Received check box...! ");
			logger.info("Clicked on payment received check box...!");
			Thread.sleep(2000);

			CashPayment.Click_ConfirmBtn();
			extentTest.info("Click Confirm Button...!");
			logger.info("Clicked on Confirm Button...!");
			Thread.sleep(3000);

			//get the booking Id
			String BookingId=BookingCreation.getBookingId(driver);
			System.out.println("Booking Id is:-" + BookingId);

			//get booking status
			String Status=upfront0.getText_BookingStatus();
			//Assertion applied to verify status of the booking after payment
			Assert.assertEquals(Status, "Successful", "Status of the booking is");
			extentTest.info(MarkupHelper.createLabel("Booking status assertion is passed...!",
					ExtentColor.GREEN));
			System.out.println("Booking status assertion is passed...!");
			logger.info("Booking status assertion is passed...!");

			//get time on which booking is created
			String GetTime=EditDate.getDate();

			EditDate.Click_Submit();
			extentTest.info("Click Submit Button...!");
			logger.info("Click on submit button...!");
			Thread.sleep(5000);

			extentTest.info(MarkupHelper.createLabel("Booking created sucessfully...!", ExtentColor.ORANGE));
			System.out.println("Booking created successfully...!");
			logger.info("Booking created successfully...!");

			//click created booking for update Time
			// click booking created on 5 am--> 8 th slot
			//			EditTime_UpdateSame.Click_Created_Booking(driver);
			//			extentTest.info("Click Created booking for Edit");
			//			logger.info("Clicked on Created booking to edit time");
			//			Thread.sleep(5000);

			//click on view reservation button
			EditEvent.Click_ViewReservation_Btn();
			extentTest.info("Click View Reservation Button...!");
			logger.info("Clicked on View Reservation button...!");
			Thread.sleep(3000);

			BookingCreation.EnterBookingId(BookingId);
			Thread.sleep(3000);


			EditEvent.Click_BookingArrow();
			extentTest.info("Click on booking arrow...!");
			logger.info("Clicked on Booking arrow...!");
			Thread.sleep(3000);

			EditDate.Click_EditButton();
			extentTest.info("Click Edit Button...!");
			logger.info("Clicked on edit button...!");
			Thread.sleep(5000);

			//10 Am
			EditTime.Edit_Time(driver, A_Utility_Class.getdatafromEs(1, 0, "EditTime"));
			extentTest.info("Select New Time To Edit...!");
			logger.info("New time selected for booking...!");
			Thread.sleep(5000);

			EditDate.Click_UpdateBooking();
			extentTest.info("Click Upbadate Booking Button...!");
			logger.info("Clicked on Update Booking Button...!");
			Thread.sleep(5000);

			EditDate.Click_ConfirmEdit();
			extentTest.info("Click Confirm Button...!");
			logger.info("Clicked on Confirm Button...!");

			//get time from summurry screen
			String UpdatedTime=EditDate.getDate();

			//Assertion applied to verify booking updated on expected time 
			Assert.assertEquals(GetTime, UpdatedTime, "Updated Time of the booking is");
			extentTest.info(MarkupHelper.createLabel("Updated time verifying assertion is passed...!",
					ExtentColor.GREEN));
			System.out.println("Updated time verifying assertion is passed...!");
			logger.info("Updated time verifying assertion is passed...!");

			EditDate.Click_Submit();
			extentTest.info("Click Submit Button...!");
			logger.info("Click on submit Button...!");

			extentTest.info(MarkupHelper.createLabel("Booking Time updated successfully...!", ExtentColor.ORANGE));
			System.out.println("Booking Time updated successfully...!");
			logger.info("Booking Time updated successfully...!");

			Thread.sleep(5000);

			//click updated booking- To update the free slot on same time
			// click created booking updated on 10 am--> 2nd slot
			//			EditTime_UpdateSame.Click_Updated_Booking(driver);
			//			extentTest.info("Click updated booking to book free slot on same time ");
			//			logger.info("Clicked on Created booking to book free slot on same time");
			//			Thread.sleep(5000);

			//click on view reservation button
			EditEvent.Click_ViewReservation_Btn();
			extentTest.info("Click View Reservation Button...!");
			logger.info("Clicked on View Reservation button...!");
			Thread.sleep(3000);

			BookingCreation.EnterBookingId(BookingId);
			Thread.sleep(3000);


			EditEvent.Click_BookingArrow();
			extentTest.info("Click on booking arrow...!");
			logger.info("Clicked on Booking arrow...!");
			Thread.sleep(3000);

			EditDate.Click_EditButton();
			extentTest.info("Click Edit Button...!");
			logger.info("Clicked on edit button...!");
			Thread.sleep(5000);

			EditTime.Select_PreviousTime(driver,  A_Utility_Class.getdatafromEs(1, 1, "EditTime"));
			extentTest.info("Select Previous Time To Check Free Slot");
			logger.info("Previous time selected to move the booking...!");
			Thread.sleep(5000);

			EditDate.Click_UpdateBooking();
			extentTest.info("Click Upbadate Booking Button...!");
			logger.info("Clicked on Update Booking Button...!");
			Thread.sleep(5000);

			EditDate.Click_ConfirmEdit();
			extentTest.info("Click Confirm Button...!");
			logger.info("Clicked on Confirm Button...!");
			Thread.sleep(3000);

			//get previous time on which booking is created
			String PreviousTime=EditDate.getDate();

			//Assertion applied to verify booking created on expected time 
			Assert.assertEquals(GetTime, PreviousTime, "Newely created booking time is");
			extentTest.info(MarkupHelper.createLabel("Newely created booking time verifying assertion is passed...!",
					ExtentColor.GREEN));
			System.out.println("Newely created booking time verifying assertion is passed...!");
			logger.info("Newely created booking time verifying assertion is passed...!");



			EditDate.Click_Submit();
			logger.info("Click on submit Button...!");
			extentTest.info("Click Submit Button...!");

			extentTest.info(MarkupHelper.createLabel("Successfully update the booking on previous time...!", ExtentColor.ORANGE));
			System.out.println("Successfully update the booking on previous time...!");
			logger.info("Successfully update the booking on previous time...!");

		}
		catch(Exception e) 
		{
			e.getStackTrace();
			String getCause = e.getLocalizedMessage();
			System.out.println("issue cause is :"+getCause);
			extentTest.fail("TC014_EditTime_UpdateSameTime test case is fail due to" + getCause);
			logger.error("TC014_EditTime_UpdateSameTime test case is fail due to " + getCause);
		}



	}





}
