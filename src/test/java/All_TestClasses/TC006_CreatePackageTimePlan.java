package All_TestClasses;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;
import org.apache.poi.EncryptedDocumentException;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;

import Admin_Business.A_Base_Class;
import Admin_Business.A_Utility_Class;
import Admin_Business.Add_Service;
import Admin_Business.Bussiness_Login1;
import Admin_Business.CreatePackageSessionPlan;
import Admin_Business.CreatePackageTimePlan;
import Admin_Business.CreatePlan;

public class TC006_CreatePackageTimePlan extends A_Base_Class
{
	Logger logger=(Logger) LogManager.getLogger("TC006_CreatePackageTimePlan");


	Bussiness_Login1 Blogin;
	CreatePlan Createplan;
	CreatePackageSessionPlan PkgSession;
	Add_Service  AddService;
	CreatePackageTimePlan PkgTime;

	@BeforeClass
	public void Open_Brouser (ITestContext Context) throws InterruptedException, EncryptedDocumentException, IOException 
	{
		Initialise_Browser(Context);
		logger.info("Browser Opened...!");

		Blogin=new Bussiness_Login1(driver);
		Createplan=new CreatePlan(driver);
		PkgSession=new CreatePackageSessionPlan(driver);
		AddService=new Add_Service(driver);
		PkgTime=new CreatePackageTimePlan(driver);
	}

	@BeforeMethod
	public void Login() throws InterruptedException, IOException 
	{
		Blogin.Click_Radio();
		Blogin.Enter_UN(A_Utility_Class.getdatafromPF("UN_Business"));
		Blogin.Enter_PWS(A_Utility_Class.getdatafromPF("PWS_Business"));
		Blogin.Click_Login();
		Thread.sleep(5000);
		logger.info("Login credentials are entered...!");
	}

	// create Package session plan
	@Test (testName = "Create_PackageTimePlan")
	public void Create_PackageTestPlan() throws IOException, InterruptedException 
	{
		try
		{ 
			AddService.clickOutlet();
			extentTest.info("Click Outlet button");
			logger.info("Clicked on outlet button...!");
			Thread.sleep(5000);

			AddService.clickArrow(driver);
			extentTest.info("Click On Arrow");
			logger.info("Login credentials are entered...!");
			Thread.sleep(2000);

			Createplan.clickEditBtn();
			extentTest.info("Click Edit button...!");
			logger.info("Clicked on Edit button...!");
			Thread.sleep(4000);

			Createplan.clickAddMoreBtn();
			extentTest.info("Click AddMore button...!");
			logger.info("Clicked on AddMore button...!");
			Thread.sleep(5000);

			Createplan.EnterPlanName(A_Utility_Class.getdatafromEs(2, 0, "Create_Plans"));
			extentTest.info("Enter plan name...!");
			logger.info("Plan name entered...!");
			Thread.sleep(2000);
			
			Createplan.ClickUploadImgbtn();
			Thread.sleep(2000);
			Createplan.ClickUploadImgRadiobtn();
			Thread.sleep(2000);
			Createplan.ClickBrowsebtn();
			Thread.sleep(2000);
			
			A_Utility_Class.uploadimg(A_Utility_Class.getdatafromEs(2, 1, "Create_Plans"));
			Thread.sleep(2000);
			
			Createplan.ClickUploadbtn();
			Thread.sleep(2000);
			
			PkgSession.clickEnablePackageCheckBox();
			extentTest.info("Click Enable package checkBox...!");
			logger.info("Clicked on Enable package checkBox...!");
			Thread.sleep(2000);

			
			Createplan.EnterMinTimeInAdvanced(A_Utility_Class.getdatafromEs(2, 2, "Create_Plans"), driver);
			Thread.sleep(2000);
			Createplan.EnterMaxTimeInAdvanced(A_Utility_Class.getdatafromEs(2, 3, "Create_Plans"), driver);
			Thread.sleep(2000);
			extentTest.info("Enter timeInAdvanced...!");
			logger.info("TimeInAdvanced entered...!");


			Createplan.SelectDays();
			extentTest.info("Days are selected...!");
			logger.info("Days are selected...!");
			Thread.sleep(5000);

			Createplan.SelectServiceCategory();
			extentTest.info("Service Category is selected...!");
			logger.info("Service Category is selected...!");
			Thread.sleep(2000);

			Createplan.SelectVenueType();
			extentTest.info("Venue type is selected...!");
			logger.info("Venue type is selected...!");
			Thread.sleep(2000);

			Createplan.SelectServiceDescription();
			extentTest.info("Service Description is selected...!");
			logger.info("Service Description is selected...!");
			Thread.sleep(2000);

			Createplan.SelectBookingType();
			extentTest.info("Booking type is selected...!");
			logger.info("Booking type is selected...!");
			Thread.sleep(2000);

			Createplan.SelectFrom();
			Thread.sleep(2000);
			Createplan.SelectTo();
			extentTest.info("From and To are selected...!");
			logger.info("From and To are selected...!");
			Thread.sleep(2000);

			PkgSession.ClickPriceperslotperhour();
			extentTest.info("Click Priceperslotperhour checkBox...!");
			logger.info("Click Priceperslotperhour checkBox...!");
			Thread.sleep(5000);	
	        System.out.println("Click Priceperslotperhour checkBox...!");
			
			
     		PkgTime.clickTimeRangeRadiobtn(driver);
     		System.out.println("Click TimeRange radiobtn...!");
			PkgTime.selectBookingDuration();
			System.out.println("Select bookings duration...!");
			extentTest.info("TimeRange and Booking Duration is selected ...!");
			logger.info("TimeRange and Booking Duration is selected...!");
			Thread.sleep(2000);	
			
			PkgSession.EnterMaxGuestPerSlot(A_Utility_Class.getdatafromEs(2, 4, "Create_Plans"));
			PkgSession.EnterGuestPerPackage(A_Utility_Class.getdatafromEs(2, 5, "Create_Plans"), driver);
			PkgSession.EnterMaxPackagesAllowed(A_Utility_Class.getdatafromEs(2, 6, "Create_Plans"), driver);
			PkgSession.EnterMaxGuestsAllowed(A_Utility_Class.getdatafromEs(2, 7, "Create_Plans"));
			
			extentTest.info("Enter Max Guest per slot/Guest allowed...!");
			logger.info("Entered Max Guest per slot/Guest allowed...!");
		
            Createplan.EnterPurchaseItemsName(A_Utility_Class.getdatafromEs(2, 8, "Create_Plans"));
			Thread.sleep(2000);
			Createplan.EnterPurchaseItemsprice(A_Utility_Class.getdatafromEs(2, 9, "Create_Plans"));
			Thread.sleep(2000);
			Createplan.EnterPurchaseItemSaleTax(A_Utility_Class.getdatafromEs(2, 10, "Create_Plans"));
			Thread.sleep(2000);
			Createplan.SelectPurchaseItemTimeFrame();
			extentTest.info("Purchase item details enter...!");
			logger.info("Purchase item details entered...!");
			Thread.sleep(2000);

			//Start Time-Slot1
			Createplan.Slot1StartTimehourSelect(A_Utility_Class.getdatafromEs(2, 11, "Create_Plans"), driver);
			Createplan.Slot1StartTimeMinuteSelect(A_Utility_Class.getdatafromEs(2, 12, "Create_Plans"), driver);
			Createplan.Slot1StartTimeampmDropdownSelect(A_Utility_Class.getdatafromEs(2, 13, "Create_Plans"), driver);
		

			//End Time-Slot1
			Createplan.Slot1EndTimehourSelect(A_Utility_Class.getdatafromEs(2, 14, "Create_Plans"), driver);
			Createplan.Slot1EndTimeMinuteSelect(A_Utility_Class.getdatafromEs(2, 12, "Create_Plans"), driver);
			Createplan.Slot1EndTimeampmDropdownSelect(A_Utility_Class.getdatafromEs(2, 13, "Create_Plans"), driver);
			PkgTime.selectTimeFrame(driver);
			
			PkgSession.EnterPackageSlot1Price(A_Utility_Class.getdatafromEs(2, 15, "Create_Plans"));
			Thread.sleep(2000);
			PkgSession.EnterPackageSlot1SaleTax(A_Utility_Class.getdatafromEs(2, 16, "Create_Plans"));
			Thread.sleep(2000);
			PkgSession.EnterAdditionalGuestSlot1Price(A_Utility_Class.getdatafromEs(2, 17, "Create_Plans"));
		
			extentTest.info("First reservation slot details enter...!");
			logger.info("First reservation slot details enter...!");
			Thread.sleep(2000);

			Createplan.ClickRefundYes();
			Thread.sleep(2000);
			Createplan.SelectRefundType();
			extentTest.info("Select refund type...!");
			logger.info("Refund type selected...!");
			Thread.sleep(2000);

			Createplan.SelectProcessingFeeCheckBox();
			Thread.sleep(2000);
			Createplan.SelectFeePer();
			Thread.sleep(2000);
			Createplan.SelectBasedOn();
			Thread.sleep(2000);
			Createplan.EnterProcessingFeeName(A_Utility_Class.getdatafromEs(2, 23, "Create_Plans"));
			Thread.sleep(2000);
			Createplan.EnterProcessingFeeAmount(A_Utility_Class.getdatafromEs(2, 24, "Create_Plans"), driver);
			extentTest.info("Processing fees details enter...!");
			logger.info("Entered Processing fees details...!");
			Thread.sleep(2000);

			Createplan.ClickCreatebtn();
			extentTest.info("Click created button...!");
			logger.info("Clicked on created button...!");
			Thread.sleep(2000);

			//Assertion
			String conformationMsg=driver.findElement(By.xpath("//div[@id='notistack-snackbar']"))
					.getText();
			System.out.println(conformationMsg);

			Assert.assertEquals(conformationMsg, "Service item has been successfully saved..!");
			extentTest.info(MarkupHelper.createLabel("Create Package Session Plan assertion is passed...!",
					ExtentColor.GREEN));
			System.out.println("Create Package Session plan assertion is passed...!");
			logger.info("Create Package Session plan assertion is passed...!");


			System.out.println("Package Session plan created successfull...!");
			extentTest.info(MarkupHelper.createLabel("Package Session plan created successfull...!", ExtentColor.ORANGE));
			logger.info("Package Session plan created successfull...!");

		}

		catch(Exception e) 
		{
			e.getStackTrace();
			String getCause = e.getLocalizedMessage();
			System.out.println("issue cause is :"+getCause);
			extentTest.fail("TC006_CreatePackageTimePlan test case is fail due to" + getCause);
			logger.error("TC006_CreatePackageTimePlan test case is fail due to " + getCause);
			
			 Assert.fail("Test case failed due to exception: " + getCause);
		}

	}






}
