package Admin_Business;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Block_SingleSlot_Createbooking
{
	
	@FindBy(xpath="//img[@alt='Slot party']") 
	private WebElement   selectPlan;
	
	@FindBy(xpath="/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[2]/div[1]/form[1]/div[1]/div[3]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/td[3]/div[1]/div[1]/div[1]/table[1]/tbody[1]/tr[2]/td[1]/div[1]/div[1]/div[1]/div[1]")
	private WebElement   blockSlot;


	public Block_SingleSlot_Createbooking (WebDriver driver)
	{ 
		PageFactory.initElements(driver, this);
	}
	
	
  public void selectPlan() 
  {
	  selectPlan.click();
  }
	

  public void ClickBlockSlot() 
  {
	  blockSlot.click();
  }
  

}
