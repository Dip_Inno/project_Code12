package Admin_Business;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CreatePackageTimePlan {

	@FindBy(xpath="//input[@value='range']")
	private WebElement   TimeRange;

	@FindBy(xpath="//input[@id='bookingduration']")
	private WebElement  BookingDuration ;

	@FindBy(xpath="//li[@id='bookingduration-option-0']//input[@type='checkbox']")
	private List<WebElement>  BookingDurationOptions ;

	@FindBy(xpath="//select[@name='schedules.0.reservationSlots.0.increment']")
	private WebElement  TimeFrame ;

	public CreatePackageTimePlan (WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}


	public void clickTimeRangeRadiobtn(WebDriver driver) throws InterruptedException
	{
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", TimeRange);
	}

	public void selectBookingDuration() throws InterruptedException 
	{
		BookingDuration.click();

		for (WebElement option : BookingDurationOptions) {
			option.click();
			Thread.sleep(200);
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}


	}


	public void selectTimeFrame(WebDriver driver) throws InterruptedException 
	{
		Select s1=new Select(TimeFrame);
		s1.selectByIndex(3);
	}



}
