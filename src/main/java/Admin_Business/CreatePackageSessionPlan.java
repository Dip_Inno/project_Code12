package Admin_Business;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CreatePackageSessionPlan {

	@FindBy(xpath="//input[@name='packageStatus']")
	private WebElement   EnablePackageCheckBox;
	
	@FindBy(xpath="//input[@name='schedules.0.perHour.visible']")
	private WebElement   Priceperslotperhour;
	
	@FindBy(xpath="//input[@name='schedules.0.maxGuest']")
	private WebElement   MaxGuestPerSlot;
	
	@FindBy(xpath="//input[@name='schedules.0.guestPerPackage']")
	private WebElement   GuestPerPackage;
	
	@FindBy(xpath="//input[@name='schedules.0.maxPackageAllowed']")
	private WebElement   MaxPackagesAllowed;
	
	@FindBy(xpath="//input[@name='schedules.0.maxGuestAllowed']")
	private WebElement   MaxGuestsAllowed;
	
	@FindBy (xpath="//input[@name='schedules.0.reservationSlots.0.pricePerHours.0.price']")
	private WebElement PackageSlot1Price ;
	
	@FindBy (xpath="//input[@name='schedules.0.reservationSlots.1.pricePerHours.0.price']")
	private WebElement PackageSlot2Price ;
	
	@FindBy (xpath="//input[@name='schedules.0.reservationSlots.0.pricePerHours.1.price']")
	private WebElement AdditionalGuestSlot1Price ;
	
	@FindBy (xpath="//input[@name='schedules.0.reservationSlots.1.pricePerHours.1.price']")
	private WebElement AdditionalGuestSlot2Price ;
	
	@FindBy (xpath="//input[@name='schedules.0.reservationSlots.0.pricePerHours.0.tax']")
	private WebElement PackageSlot1SaleTax ;
	
	@FindBy (xpath="//input[@name='schedules.0.reservationSlots.1.pricePerHours.1.tax']")
	private WebElement AdditionalGuestSlot2SaleTax ;
	
	
	public CreatePackageSessionPlan (WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}
	
	public void clearInputField(WebElement element, WebDriver driver) {
	    JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
	    jsExecutor.executeScript("arguments[0].value = '';", element);
	}

	
	public void clickEnablePackageCheckBox() throws InterruptedException 
	{
		EnablePackageCheckBox.click();
	}
	
	public void ClickPriceperslotperhour() throws InterruptedException 
	{
		Priceperslotperhour.click();
	}
	
	public void EnterMaxGuestPerSlot(String NoOfslot) throws InterruptedException 
	{
		MaxGuestPerSlot.sendKeys(NoOfslot);
	}
	
	public void EnterGuestPerPackage(String NoOfGuest, WebDriver driver) throws InterruptedException 
	{
		GuestPerPackage.click();
		clearInputField(GuestPerPackage, driver);
		GuestPerPackage.sendKeys(NoOfGuest);
	}
	
	public void EnterMaxPackagesAllowed(String NoOfPackage, WebDriver driver) throws InterruptedException 
	{
		MaxPackagesAllowed.click();
		clearInputField(MaxPackagesAllowed, driver);
		MaxPackagesAllowed.sendKeys(NoOfPackage);
	}
	
	public void EnterMaxGuestsAllowed(String NoOfGuest) throws InterruptedException 
	{
		MaxGuestsAllowed.sendKeys(NoOfGuest);
	}
	
	public void EnterPackageSlot1Price(String Price) throws InterruptedException 
	{
		PackageSlot1Price.sendKeys(Price);
	}
	
	public void EnterPackageSlot2Price(String price) throws InterruptedException 
	{
		PackageSlot2Price.sendKeys(price);
	}
	
	public void EnterAdditionalGuestSlot1Price(String Price) throws InterruptedException 
	{
		AdditionalGuestSlot1Price.sendKeys(Price);
	}
	
	public void EnterAdditionalGuestSlot2Price(String price) throws InterruptedException 
	{
		AdditionalGuestSlot2Price.sendKeys(price);
	}
	
	public void EnterPackageSlot1SaleTax(String Tax) throws InterruptedException 
	{
		PackageSlot1SaleTax.sendKeys(Tax);
	}
	
	public void EnterAdditonalGuestSlot2SaleTax(String Tax) throws InterruptedException 
	{
		AdditionalGuestSlot2SaleTax.sendKeys(Tax);
	}
	
	
}
