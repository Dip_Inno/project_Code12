package Admin_Business;

import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class CreatePlan {
	

	@FindBy(xpath="//body/div[@id='root']/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/form[1]/div[1]/div[1]/div[2]/div[1]/div[2]/span[1]")
	private WebElement   UploadImg;
	

	@FindBy(xpath="//span//input[@name='Image']")
	private WebElement   UploadImgRadiobtn;
	

	@FindBy(xpath="//body/div[3]/div[3]/div[1]/div[1]/div[2]/div[1]/p[1]/fieldset[1]/div[1]/div[1]/div[2]/label[1]/span[1]/span[1]")
	private WebElement   Browsebtn;
	

	@FindBy(xpath="//body/div[3]/div[3]/div[1]/div[2]/div[1]/button[2]/span[1]")
	private WebElement   Uploadbtn;

	@FindBy(xpath="//button//span//span[@class='MuiButton-startIcon MuiButton-iconSizeSmall css-y6rp3m-MuiButton-startIcon']")
	private WebElement   Editbtn;

	@FindBy(xpath="/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[1]/div[1]/button[1]/span[1]")
	private WebElement   AddMoretbtn;

	@FindBy(xpath="//input[@name='title']")
	private WebElement   EnterPlanName;

	@FindBy(xpath="//input[@name='minimumTimeAdvance']")
	private WebElement   MinTimeInAdvanced;

	@FindBy(xpath="//input[@name='maximumTimeAdvance']")
	private WebElement   MaxTimeInAdvanced;

	@FindBy(xpath="//input[@id='days']")
	private WebElement   SelectDays;

	@FindBy(xpath="//li[contains(@class, 'MuiAutocomplete-option')]")
	private List<WebElement> dropDownOptions;

	@FindBy(xpath="//select[@name='schedules.0.venueDetail.0.serviceCategory']")
	private WebElement   SelectServiceCategory;

	@FindBy(xpath="//select[@name='schedules.0.venueDetail.0.venueType']")
	private WebElement   SelectVenueType;

	@FindBy(xpath="//select[@name='schedules.0.venueDetail.0.serviceDescription']")
	private WebElement   SelectServiceDescription;

	@FindBy(xpath="//select[@name='schedules.0.venueDetail.0.bookingType']")
	private WebElement   SelectBookingType;

	@FindBy(xpath="//select[@name='schedules.0.venueDetail.0.from']")
	private WebElement   SelectFrom;

	@FindBy(xpath="//select[@name='schedules.0.venueDetail.0.to']")
	private WebElement   SelectTo;

	@FindBy(xpath="//input[@name='schedules.0.purchaseItems.0.name']")
	private WebElement   PurchaseItemsName;

	@FindBy(xpath="//input[@name='schedules.0.purchaseItems.0.price']")
	private WebElement   PurchaseItemsprice;

	@FindBy(xpath="//input[@name='schedules.0.purchaseItems.0.tax']")
	private WebElement   PurchaseItemSaleTax;

	@FindBy(xpath="//select[@name='schedules.0.purchaseItems.0.timeFrame']")
	private WebElement   PurchaseItemTimeFrame;

	@FindBy (xpath="//body/div[@id='root']/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/form[1]/div[1]/div[1]/div[17]/div[1]/div[3]/div[4]/div[1]/form[1]/div[1]/div[1]/div[1]/input[1]")
	private WebElement Slot1StartTimehour ;

	@FindBy (xpath="//body/div[@id='root']/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/form[1]/div[1]/div[1]/div[17]/div[1]/div[3]/div[4]/div[1]/form[1]/div[2]/div[1]/div[1]/input[1]")
	private WebElement Slot1StartTimeMinute ;

	@FindBy (xpath="//body/div[@id='root']/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/form[1]/div[1]/div[1]/div[17]/div[1]/div[3]/div[4]/div[1]/form[1]/div[3]/div[1]/div[1]/input[1]")
	private WebElement Slot1StartTimeampmDropdown ;

	@FindBy (xpath="//body/div[@id='root']/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/form[1]/div[1]/div[1]/div[17]/div[1]/div[3]/div[5]/div[1]/form[1]/div[1]/div[1]/div[1]/input[1]")
	private WebElement Slot1EndTimehour ;

	@FindBy (xpath="//body/div[@id='root']/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/form[1]/div[1]/div[1]/div[17]/div[1]/div[3]/div[5]/div[1]/form[1]/div[2]/div[1]/div[1]/input[1]")
	private WebElement Slot1EndTimeMinute ;

	@FindBy (xpath="//body/div[@id='root']/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/form[1]/div[1]/div[1]/div[17]/div[1]/div[3]/div[5]/div[1]/form[1]/div[3]/div[1]/div[1]/input[1]")
	private WebElement Slot1EndTimeampmDropdown ;
	
	@FindBy (xpath="//body/div[@id='root']/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/form[1]/div[1]/div[1]/div[16]/div[2]/button[1]")
	private WebElement AddMoreReservationSlot ;

	@FindBy (xpath="//body/div[@id='root']/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/form[1]/div[1]/div[1]/div[17]/div[3]/div[3]/div[4]/div[1]/form[1]/div[1]/div[1]/div[1]/input[1]")
	private WebElement Slot2StartTimehour ;

	@FindBy (xpath="//body/div[@id='root']/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/form[1]/div[1]/div[1]/div[17]/div[3]/div[3]/div[4]/div[1]/form[1]/div[2]/div[1]/div[1]/input[1]")
	private WebElement Slot2StartTimeMinute ;

	@FindBy (xpath="//body/div[@id='root']/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/form[1]/div[1]/div[1]/div[17]/div[3]/div[3]/div[4]/div[1]/form[1]/div[3]/div[1]/div[1]/input[1]")
	private WebElement Slot2StartTimeampmDropdown ;

	@FindBy (xpath="//body/div[@id='root']/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/form[1]/div[1]/div[1]/div[17]/div[3]/div[3]/div[5]/div[1]/form[1]/div[1]/div[1]/div[1]/input[1]")
	private WebElement Slot2EndTimehour ;

	@FindBy (xpath="//body/div[@id='root']/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/form[1]/div[1]/div[1]/div[17]/div[3]/div[3]/div[5]/div[1]/form[1]/div[2]/div[1]/div[1]/input[1]")
	private WebElement Slot2EndTimeMinute ;

	@FindBy (xpath="//body/div[@id='root']/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/form[1]/div[1]/div[1]/div[17]/div[3]/div[3]/div[5]/div[1]/form[1]/div[3]/div[1]/div[1]/input[1]")
	private WebElement Slot2EndTimeampmDropdown ;

	@FindBy (xpath="//span[@class='MuiButtonBase-root MuiIconButton-root MuiIconButton-colorPrimary MuiIconButton-sizeMedium MuiRadio-root MuiRadio-colorPrimary PrivateSwitchBase-root css-1sgfnwh-MuiButtonBase-root-MuiIconButton-root-MuiRadio-root']//input[@name='schedules.0.bookingDepositDetails.refundStatus']")
	private WebElement RefundRadiobtn ;

	@FindBy (xpath="//select[@name='schedules.0.bookingDepositDetails.refundType']")
	private WebElement RefundType ;

	@FindBy (xpath="//input[@name='processingFeeDetails.details.0.status']")
	private WebElement ProcessingFeeCheckBox ;

	@FindBy (xpath="//select[@name='processingFeeDetails.details.0.type']")
	private WebElement ProcessingFeePer ;

	@FindBy (xpath="//input[@id='taxapplied']")
	private WebElement BookingTotalDropdown ;

	@FindBy (xpath="//li[contains(text(),'Total')]")
	private WebElement TotalOption ;

	@FindBy (xpath="//input[@name='processingFeeDetails.details.0.name']")
	private WebElement ProcessingFeeName ;

	@FindBy (xpath="//input[@name='processingFeeDetails.details.0.amountPer']")
	private WebElement ProcessingFeeAmount ;

	@FindBy (xpath="//body/div[@id='root']/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/form[1]/div[1]/div[1]/div[1]/button[1]")
	private WebElement Createbtn ;

	public CreatePlan (WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}
	
	public void clearInputField(WebElement element, WebDriver driver) {
	    JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
	    jsExecutor.executeScript("arguments[0].value = '';", element);
	}

	public void clickEditBtn() throws InterruptedException 
	{
		Editbtn.click();
	}

	public void clickAddMoreBtn() throws InterruptedException 
	{
		AddMoretbtn.click();
	}

	public void EnterPlanName(String name) throws InterruptedException 
	{
		EnterPlanName.sendKeys(name);
	}
	
	public void ClickUploadImgbtn() throws InterruptedException 
	{
		UploadImg.click();
	}
	
	public void ClickUploadImgRadiobtn() throws InterruptedException 
	{
		UploadImgRadiobtn.click();
	}
	
	public void ClickBrowsebtn() throws InterruptedException 
	{
		Browsebtn.click();
	}
	
	public void ClickUploadbtn() throws InterruptedException 
	{
		Uploadbtn.click();
	}

	public void EnterMinTimeInAdvanced(String time , WebDriver driver) throws InterruptedException 
	{
		MinTimeInAdvanced.click();
		clearInputField(MinTimeInAdvanced, driver);
		MinTimeInAdvanced.sendKeys(time);
	}

	public void EnterMaxTimeInAdvanced(String Time, WebDriver driver) throws InterruptedException 
	{
		MaxTimeInAdvanced.click();
		clearInputField(MaxTimeInAdvanced, driver);
		MaxTimeInAdvanced.sendKeys(Time);
	}

	public void SelectDays() throws InterruptedException 
	{
		SelectDays.click();

		for (WebElement option : dropDownOptions) {
			option.click();
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}

	public void SelectServiceCategory() throws InterruptedException 
	{
		Select s1=new Select(SelectServiceCategory);
		s1.selectByIndex(1);
	}

	public void SelectVenueType() throws InterruptedException 
	{
		Select s1=new Select(SelectVenueType);
		s1.selectByIndex(1);
	}

	public void SelectServiceDescription() throws InterruptedException 
	{
		Select s1=new Select(SelectServiceDescription);
		s1.selectByIndex(1);
	}


	public void SelectBookingType() throws InterruptedException 
	{
		Select s1=new Select(SelectBookingType);
		s1.selectByIndex(1);
	}

	public void SelectFrom() throws InterruptedException 
	{
		Select s1=new Select(SelectFrom);
		s1.selectByIndex(1);
	}

	public void SelectTo() throws InterruptedException 
	{
		Select s1=new Select(SelectTo);
		s1.selectByIndex(10);
	}


	public void EnterPurchaseItemsName(String ItemName) throws InterruptedException 
	{
		PurchaseItemsName.sendKeys(ItemName);
	}

	public void EnterPurchaseItemsprice(String ItemPrice) throws InterruptedException 
	{
		PurchaseItemsprice.sendKeys(ItemPrice);
	}

	public void EnterPurchaseItemSaleTax(String ItemTax) throws InterruptedException 
	{
		PurchaseItemSaleTax.sendKeys(ItemTax);
	}

	public void SelectPurchaseItemTimeFrame() throws InterruptedException 
	{
		Select s1=new Select(PurchaseItemTimeFrame);
		s1.selectByIndex(1);
	}

	public void Slot1StartTimehourSelect(String time, WebDriver driver) throws InterruptedException 
	{
       Actions A1=new Actions(driver);
		A1.click(Slot1StartTimehour).perform();
		A1.sendKeys(time).perform();
		A1.sendKeys(Keys.ENTER).perform();
		Thread.sleep(3000);
	}

	public void Slot1StartTimeMinuteSelect(String minute, WebDriver driver) throws InterruptedException 
	{
		 Actions A1=new Actions(driver);
		A1.click(Slot1StartTimeMinute).perform();
		A1.sendKeys(minute).perform();
		A1.sendKeys(Keys.ENTER).perform();
		Thread.sleep(3000);
	}

	public void Slot1StartTimeampmDropdownSelect(String ampm, WebDriver driver) throws InterruptedException 
	{
		Actions A1=new Actions(driver);
		A1.click(Slot1StartTimeampmDropdown).perform();
		clearInputField(Slot1StartTimeampmDropdown, driver);
		A1.sendKeys(ampm).perform();
		A1.sendKeys(Keys.ENTER).perform();
		Thread.sleep(3000);
		
		
	}


	public void Slot1EndTimehourSelect(String time, WebDriver driver) throws InterruptedException 
	{
		Actions A1=new Actions(driver);
		A1.click(Slot1EndTimehour).perform();
		A1.sendKeys(time).perform();
		A1.sendKeys(Keys.ENTER).perform();
		Thread.sleep(3000);
	}

	public void Slot1EndTimeMinuteSelect(String minute, WebDriver driver) throws InterruptedException 
	{
		Actions A1=new Actions(driver);
		A1.click(Slot1EndTimeMinute).perform();
		A1.sendKeys(minute).perform();
		A1.sendKeys(Keys.ENTER).perform();
		Thread.sleep(3000);
	}

	public void Slot1EndTimeampmDropdownSelect(String ampm, WebDriver driver) throws InterruptedException 
	{
		Actions A1=new Actions(driver);
		A1.click(Slot1EndTimeampmDropdown).perform();
		clearInputField(Slot1EndTimeampmDropdown, driver);
		A1.sendKeys(ampm).perform();
		A1.sendKeys(Keys.ENTER).perform();
		Thread.sleep(3000);
	}
	
	public void AddMoreReservationSlot() throws InterruptedException 
	{
		AddMoreReservationSlot.click();
		Thread.sleep(2000);
	}


	public void Slot2StartTimehourSelect(String time, WebDriver driver) throws InterruptedException 
	{
		Actions A1=new Actions(driver);
		A1.click(Slot2StartTimehour).perform();
		A1.sendKeys(time).perform();
		A1.sendKeys(Keys.ENTER).perform();
		Thread.sleep(3000);
	}
    public void Slot2StartTimeMinuteSelect(String minute, WebDriver driver) throws InterruptedException 
	{
		Actions A1=new Actions(driver);
		A1.click(Slot2StartTimeMinute).perform();
		A1.sendKeys(minute).perform();
		A1.sendKeys(Keys.ENTER).perform();
		Thread.sleep(2000);
	}

	public void Slot2StartTimeampmDropdownSelect(String ampm, WebDriver driver) throws InterruptedException 
	{
		Actions A1=new Actions(driver);
		A1.click(Slot2StartTimeampmDropdown).perform();
		clearInputField(Slot2StartTimeampmDropdown, driver);
		A1.sendKeys(ampm).perform();
		A1.sendKeys(Keys.ENTER).perform();
		Thread.sleep(2000);
	}


	public void Slot2EndTimehourSelect(String time, WebDriver driver) throws InterruptedException 
	{
		Actions A1=new Actions(driver);
		A1.click(Slot2EndTimehour).perform();
		A1.sendKeys(time).perform();
		A1.sendKeys(Keys.ENTER).perform();
		Thread.sleep(3000);
	}


	public void Slot2EndTimeMinuteSelect(String minute, WebDriver driver) throws InterruptedException 
	{
		Actions A1=new Actions(driver);
		A1.click(Slot2EndTimeMinute).perform();
		A1.sendKeys(minute).perform();
		A1.sendKeys(Keys.ENTER).perform();
		Thread.sleep(3000);
	}


	public void Slot2EndTimeampmDropdownSelect(String ampm, WebDriver driver) throws InterruptedException 
	{
		Actions A1=new Actions(driver);
		A1.click(Slot2EndTimeampmDropdown).perform();
		clearInputField(Slot2StartTimeampmDropdown, driver);
		A1.sendKeys(ampm).perform();
		A1.sendKeys(Keys.ENTER).perform();
		Thread.sleep(3000);
	}

	public void ClickRefundYes() throws InterruptedException 
	{
		RefundRadiobtn.click();
	}


	public void SelectRefundType() throws InterruptedException 
	{
		Select s1=new Select(RefundType);
		s1.selectByIndex(2);
	}

	public void SelectProcessingFeeCheckBox() throws InterruptedException 
	{
		ProcessingFeeCheckBox.click();
	}

	public void SelectFeePer() throws InterruptedException 
	{
		Select s1=new Select(ProcessingFeePer);
		s1.selectByIndex(1);
	}

	public void SelectBasedOn() throws InterruptedException 
	{
		BookingTotalDropdown.click();
		TotalOption.click();
	}

	public void EnterProcessingFeeName(String name) throws InterruptedException 
	{
		ProcessingFeeName.sendKeys(name);
	}

	public void EnterProcessingFeeAmount(String Fees, WebDriver driver) throws InterruptedException 
	{
		ProcessingFeeAmount.click();
		clearInputField(ProcessingFeeAmount, driver);
		ProcessingFeeAmount.sendKeys(Fees);
	}

	public void ClickCreatebtn() throws InterruptedException 
	{
		Createbtn.click();
	}

}
