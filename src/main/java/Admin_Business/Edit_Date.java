package Admin_Business;

<<<<<<< HEAD
import java.time.Duration;

=======
import org.openqa.selenium.By;
>>>>>>> origin/master
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
<<<<<<< HEAD
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
=======
>>>>>>> origin/master


public class Edit_Date 
{
<<<<<<< HEAD


	@FindBy(xpath="//button//span[text()='Submit']") 
	private WebElement   Submit;

	//update the text of created Booking before execution according to name given to the booking
	@FindBy(xpath="(//div//a[contains(@class,'fc-timeline-even')])[4]")
	private  WebElement   ClickCreatedBooking;

	@FindBy(xpath="//button//span[text()='Edit']") 
	private WebElement   EditButton;

	@FindBy(xpath="/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[2]/div[1]/form[1]/div[1]/div[3]/div[2]/form[1]/div[1]/form[1]/div[1]/div[2]/div[3]/div[1]/div[1]/div[1]/button[1]/span[1]/*[name()='svg'][1]")
	private  WebElement CalenderEdit;

	@FindBy(xpath="//button[text()='12']") 
	private WebElement   EditDate;
	
	@FindBy(xpath="(//div//p[contains(@class,'MuiTypography-root MuiTypography-body2 css-1im')])[2]") 
	private WebElement getdate;

	@FindBy(xpath="//button//span[text()='Update Booking']")
	private  WebElement   UpdateBooking;

	@FindBy(xpath="//button//span[text()='Confirm']") 
	private WebElement   ConfermEdit;

	@FindBy(xpath="(//div//td[contains(@class,'fc-timeline-slot fc-timeline-s')])[9]")
	private  WebElement   ScrollToBooking;


	public Edit_Date (WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}

	public void Click_Submit() throws InterruptedException 
	{
		Submit.click();
		Thread.sleep(2000);
	}

	public void Click_Created_Booking(WebDriver driver) throws InterruptedException 
	{
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView()", ScrollToBooking);
		Thread.sleep(2000);

		ClickCreatedBooking.click();
		Thread.sleep(2000);
	}

	public void Click_EditButton() throws InterruptedException 
	{
		EditButton.click();
		Thread.sleep(2000);
	}

	public void Click_CalenderEdit(WebDriver driver) throws InterruptedException 
	{
		WebDriverWait wait=new WebDriverWait(driver, Duration.ofSeconds(30));
		wait.until(ExpectedConditions.elementToBeClickable(CalenderEdit));
		CalenderEdit.click();
		
		Thread.sleep(2000);
	}

	public void EditDate() throws InterruptedException 
	{
		EditDate.click();
		Thread.sleep(2000);
	}

	
	
	public void Click_UpdateBooking() throws InterruptedException 
	{
		UpdateBooking.click();
		Thread.sleep(2000);
	}
	public void Click_ConfirmEdit() throws InterruptedException 
	{
		ConfermEdit.click();
		Thread.sleep(2000);
	}

	public String getDate() 
	{
		String gd=getdate.getText();
		System.out.println("Booking date is-" + gd);
		return gd;
	}
=======
	
	
	@FindBy(xpath="//button//span[text()='Submit']") WebElement   Submit;
	
	//update the text of created Booking before execution according to name given to the booking
	@FindBy(xpath="(//div//a[contains(@class,'fc-timeline-even')])[4]") WebElement   ClickCreatedBooking;
	
	@FindBy(xpath="//button//span[text()='Edit']") WebElement   EditButton;
	@FindBy(xpath="(//button[contains(@class,'MuiButtonBase-root Mui')])[20]") WebElement CalenderEdit;
	@FindBy(xpath="//button[text()='12']") WebElement   EditDate;
	@FindBy(xpath="//button//span[text()='Update Booking']") WebElement   UpdateBooking;
	@FindBy(xpath="//button//span[text()='Confirm']") WebElement   ConfermEdit;
	@FindBy(xpath="(//div//td[contains(@class,'fc-timeline-slot fc-timeline-s')])[9]") WebElement   ScrollToBooking;
    
	
	  public Edit_Date (WebDriver driver)
		{
			PageFactory.initElements(driver, this);
		}
		
	  public void Click_Submit() throws InterruptedException 
	  {
		  Submit.click();
		  Thread.sleep(2000);
	  }
	  
	  public void Click_Created_Booking(WebDriver driver) throws InterruptedException 
	  {
		  ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView()", ScrollToBooking);
		  Thread.sleep(2000);
		  
		  ClickCreatedBooking.click();
		  Thread.sleep(2000);
	  }
	  
	  public void Click_EditButton() throws InterruptedException 
	  {
		  EditButton.click();
		  Thread.sleep(2000);
	  }
	  
	  public void Click_CalenderEdit() throws InterruptedException 
	  {
		  CalenderEdit.click();
		  Thread.sleep(2000);
	  }
	  
	  public void EditDate() throws InterruptedException 
	  {
		  EditDate.click();
		  Thread.sleep(2000);
	  }
	  
	  public void Click_UpdateBooking() throws InterruptedException 
	  {
		  UpdateBooking.click();
		  Thread.sleep(2000);
	  }
	  public void Click_ConfirmEdit() throws InterruptedException 
	  {
		  ConfermEdit.click();
		  Thread.sleep(2000);
	  }
	  
>>>>>>> origin/master

}
