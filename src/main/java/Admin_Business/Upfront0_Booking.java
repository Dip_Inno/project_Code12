package Admin_Business;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class Upfront0_Booking
{

	@FindBy(xpath="//img[@alt='Upfront 0']")
	private WebElement Upfront0Plan;



	@FindBy(xpath="//input[@name='fullPayment']")
	private WebElement UnckeckFullPay;

	@FindBy(xpath="//select[@name='status']")
	private WebElement GetStatus;



	public Upfront0_Booking(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}

	public void Select_Plan(WebDriver driver) 
	{
		Upfront0Plan.click();
	}

	public void Unckeck_FullPay() throws InterruptedException 
	{
		UnckeckFullPay.click();

	}

	public String getText_BookingStatus() 
	{
		Select s1=new Select(GetStatus);
		WebElement SelectedOption=s1.getFirstSelectedOption();
		String BookingStatus=SelectedOption.getText();
		System.out.println("Booing Status is-" + BookingStatus );

		return BookingStatus;
	}
	
	

}